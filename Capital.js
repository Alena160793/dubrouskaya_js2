const {City} = require('./City')


class Capital extends City {
    constructor(name){
        super(name)
        this.airoportLocation = ''

    }

    setAirport(location) {
        this.airoportLocation = location
    }
    
}

module.exports = {Capital}
